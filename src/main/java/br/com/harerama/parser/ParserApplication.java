package br.com.harerama.parser;

import br.com.harerama.parser.excecao.RegraNegocioExcecao;
import br.com.harerama.parser.servico.ImportacaoServico;
import br.com.harerama.parser.servico.LeituraServico;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class ParserApplication {

	public static void main(String[] args) throws RegraNegocioExcecao {
		ApplicationContext ctx = SpringApplication.run(ParserApplication.class, args);
		
		ImportacaoServico importacaoServico = ctx.getBean(ImportacaoServico.class);
		importacaoServico.importar();
		
		LeituraServico leituraServico = ctx.getBean(LeituraServico.class);
		leituraServico.realizarAnalise();
	}
}
