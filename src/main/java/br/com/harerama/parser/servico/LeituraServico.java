package br.com.harerama.parser.servico;

import br.com.harerama.parser.excecao.RegraNegocioExcecao;
import br.com.harerama.parser.modelo.Bloqueio;
import br.com.harerama.parser.modelo.Log;
import br.com.harerama.parser.repositorio.RepositorioBloqueio;
import br.com.harerama.parser.repositorio.RepositorioLog;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.apache.commons.collections4.ListUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class LeituraServico {

	@Autowired
	private ApplicationArguments argumentos;
	
	@Autowired
	private RepositorioLog repositorioLog;
	
	@Autowired
	private RepositorioBloqueio repositorioBloqueio;
	
	private Date dataInicio;
	private String duracao;
	private Integer limite;
	
	@Transactional
	public void realizarAnalise() throws RegraNegocioExcecao {
		validarArgumentos();
		
		List<Log> logs = ListUtils.emptyIfNull(repositorioLog.buscarIpsQueUltrapassaramOLimiteDeRequisicoesDentroDoPeriodo(dataInicio, gerarDataFim(), limite));
		
		for(Log log : logs){
			System.out.println(log.toString());
			Bloqueio bloqueio = new Bloqueio(log, "Access limit exceeded");
			repositorioBloqueio.deleteByLog(log);
			repositorioBloqueio.save(bloqueio);
		};

	}

	private void validarArgumentos() throws RegraNegocioExcecao {
		if (!argumentos.containsOption("startDate")) {
			throw new RegraNegocioExcecao("Required informs startDate");
		}
		if (!argumentos.containsOption("duration")) {
			throw new RegraNegocioExcecao("Required informs duration");
		}
		if (!argumentos.containsOption("threshold")) {
			throw new RegraNegocioExcecao("Required informs threshold");
		}
		
		try{
			dataInicio = new SimpleDateFormat("yyyy-MM-dd.HH:mm:ss").parse(argumentos.getOptionValues("startDate").get(0));
		}catch(ParseException e){
			throw new RegraNegocioExcecao("startDate is not a valid date by default (yyyy-MM-dd.HH:mm:ss)");
		}
		
		try{
			limite = new Integer(argumentos.getOptionValues("threshold").get(0));
		}catch(NumberFormatException e){
			throw new RegraNegocioExcecao("threshold must be a valid integer");
		}
		
		String duracao = argumentos.getOptionValues("duration").get(0);
		switch(duracao){
			case "hourly": this.duracao = "hora";
			break;
			case "daily" : this.duracao = "dia";
			break;
			default: throw new RegraNegocioExcecao("duration should be \"hourly\" or \"daily\"");
		}
		
	}

	private Date gerarDataFim() {
		Calendar c = Calendar.getInstance();
		c.setTime(dataInicio);
		c.add(Calendar.MINUTE, 59);
		c.add(Calendar.SECOND, 59);
		
		if(duracao.equals("dia")){
			c.add(Calendar.HOUR_OF_DAY, 23);
		}
		
		return c.getTime();
	}
	
}
