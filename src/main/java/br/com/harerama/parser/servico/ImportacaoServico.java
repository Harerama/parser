package br.com.harerama.parser.servico;

import br.com.harerama.parser.excecao.RegraNegocioExcecao;
import br.com.harerama.parser.modelo.Log;
import br.com.harerama.parser.repositorio.RepositorioLog;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.stereotype.Service;

@Service
public class ImportacaoServico {

	@Autowired
	private ApplicationArguments argumentos;

	@Autowired
	private RepositorioLog repositorioLog;
	
	private FileInputStream logStream;

	public void importar() throws RegraNegocioExcecao {

		if (!argumentos.containsOption("accesslog")) {
			throw new RegraNegocioExcecao("Required informs the path of the log file via named argument (accesslog)");
		}

		if(tabelaEstaPreenchida()){
			return;
		}
		
		carregarArquivo();

		CSVParser arquivo = null;

		try {
			CSVFormat formato = CSVFormat.RFC4180.withDelimiter('|').withTrim();

			arquivo = new CSVParser(new BufferedReader(new InputStreamReader(logStream, "UTF-8")), formato);

			List<CSVRecord> linhas = ListUtils.emptyIfNull(arquivo.getRecords());

			List<Log> logs = new LinkedList<>();
			for (CSVRecord linha : linhas) {
				Log log = new Log();;
				log.setData(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").parse(linha.get(0)));
				log.setIp(linha.get(1));
				log.setRequest(linha.get(2));
				log.setStatus(new Integer(linha.get(3)));
				log.setUser(linha.get(4));
				logs.add(log);
			}

			repositorioLog.deleteAll();
			repositorioLog.save(logs);
		} catch (IOException | ParseException e) {
			if(e instanceof ParseException){
				throw new RegraNegocioExcecao("The log file contains an invalid date");
			}
			e.printStackTrace();
		} finally {
			try {
				if (arquivo != null) {
					arquivo.close();
				}
			} catch (IOException e) {

			}

		}

	}

	private void carregarArquivo() throws RegraNegocioExcecao {
		try {
			String caminhoDoArquivo = argumentos.getOptionValues("accesslog").get(0);
			logStream = new FileInputStream(caminhoDoArquivo);
		} catch (FileNotFoundException e) {
			throw new RegraNegocioExcecao("Log file not found");
		}

	}

	private boolean tabelaEstaPreenchida() {
		List<Log> logs = (List<Log>) repositorioLog.findAll();
		return !ListUtils.emptyIfNull(logs).isEmpty();
	}

}
