package br.com.harerama.parser.repositorio;

import br.com.harerama.parser.modelo.Log;
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface RepositorioLog extends CrudRepository<Log, Long>{
	@Query(value = "SELECT * FROM Log WHERE data BETWEEN ?1 AND ?2 GROUP BY ip HAVING COUNT(id) > ?3", nativeQuery = true)
	public List<Log> buscarIpsQueUltrapassaramOLimiteDeRequisicoesDentroDoPeriodo(Date inicio, Date fim, Integer numeroDeRequisicoes);
}
