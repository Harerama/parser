package br.com.harerama.parser.repositorio;

import br.com.harerama.parser.modelo.Bloqueio;
import br.com.harerama.parser.modelo.Log;
import org.springframework.data.repository.CrudRepository;

public interface RepositorioBloqueio extends CrudRepository<Bloqueio, Long>{
	public void deleteByLog(Log log);
}
