package br.com.harerama.parser.excecao;

public class RegraNegocioExcecao extends Exception {

	private String msg;

	public RegraNegocioExcecao(String msg) {
		super(msg);
		this.msg = msg;
	}

	public String getMessage() {
		return msg;
	}
}
