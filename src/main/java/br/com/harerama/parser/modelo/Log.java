package br.com.harerama.parser.modelo;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Getter;
import lombok.Setter;

@Entity
public class Log {
	
	@GeneratedValue
	@Id
	@Getter @Setter
	private Long id;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Getter @Setter
	private Date data;
	
	@Getter @Setter
	private String ip;
	
	@Getter @Setter
	private String request;
	
	@Getter @Setter
	private Integer status;
	
	@Getter @Setter
	private String user;

	@Override
	public int hashCode() {
		int hash = 3;
		hash = 29 * hash + Objects.hashCode(this.id);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Log other = (Log) obj;
		if (!Objects.equals(this.id, other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(data) + "|" + request + "|" + status + "|" + user;
	}

}
