package br.com.harerama.parser.modelo;

import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import lombok.Getter;
import lombok.Setter;

@Entity
public class Bloqueio {
	
	@GeneratedValue
	@Id
	@Getter @Setter
	private Long id;
	
	@Getter @Setter
	private String comentario;
	
	@OneToOne
	@Getter @Setter
	private Log log;

	public Bloqueio() {
		
	}
	
	public Bloqueio(Log log, String comentario) {
		this.log = log;
		this.comentario = comentario;
	}
	
	@Override
	public int hashCode() {
		int hash = 7;
		hash = 47 * hash + Objects.hashCode(this.id);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Bloqueio other = (Bloqueio) obj;
		if (!Objects.equals(this.id, other.id)) {
			return false;
		}
		return true;
	}

	
}
